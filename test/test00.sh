#!/bin/bash


_shyapa_break_chars() {
  # this macro breaks a given string
  # into seperate chars and stores in char_array
  char_array=()
  while IFS='' read -r -n 1 char_eval; do
    char_array+=("$char_eval")
  done <<< "$1"
}

echo aa.aa$a
#echo $a


declare -A test_array

test_array=()



test_array[foo bar]="abc"
test_array[foo cake]="def"

echo "${test_array[foo cake]}"
echo "${test_array[@]}"

a="\""
_shyapa_break_chars $a

b=${char_array[foo]}
if [ $a = "\"" ]; then
  echo "empty"
fi

test_array=()
declare -A test_array

echo "${test_array[@]}"
