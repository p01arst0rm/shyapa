_shyapa_rem_markers() {
  # this function removes YAML markers from
  # the line array

  index=0
  _shyapa_clone_line_array
  for target_line in "${new_line_array[@]}"; do
    stripped_line=$(_shyapa_rem_ws "${target_line}")
    if [ "${stripped_line}" = "..." ] || [ "${stripped_line}" = "---" ]; then
      echo "ignoring marker found at line" $[index+1] "..."
    else
      line_array+=("${target_line}")
    fi
    index=$[index+1]
  done
}


_shyapa_rem_comments() {
  # this function removes comments from
  # the line array
  local index=0
  _shyapa_clone_line_array

  for target_line in "${new_line_array[@]}"; do
    in_comment=false
    line_read=""
    while IFS= read -r -n 1 char_eval; do
      if [ "${in_comment}" = false ]; then
        



        case "${char_eval}" in
          "#")
            if [ "${in_quote}" = "0" ]; then
              _shyapa_warn_rem_comment $[index+1]
              in_comment=true
              char_eval=
            fi;;
          "'")
            if [ "${in_quote}" != "2" ]; then
              _shyapa_toggle_in_quote "1"
            fi;;
          "\"")
            if [ "${in_quote}" != "1" ]; then
              _shyapa_toggle_in_quote "2"
            fi;;
        esac
      else
        char_eval=
      fi

      # leaving ret_line outside case allows for support
      # of /* in future
      line_read="${line_read}${char_eval}"
    done <<< "${target_line}"
    line_array+=("${line_read}")
    index=$[index+1]
  done
}