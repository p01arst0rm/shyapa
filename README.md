# shyapa
Shyapa: SHell-based YAml PArser

Shyapa is a shell based yaml parser designed for use
in bash applications.

not all features of YAML are represented exactly as represented
in the YAML specification due to the limitations of bash, and
some functions are missing entirely. If the parser encounters these
unsupported features they are ignored.

Please check that your YAML file will work with the features
available with this parser before using it.

# installation

to use this parser, you can add bin/shyapa.sh to the root of your
script, or install it in your path by running:

./configure prefix="/usr"

make

make install

execute shyapa with:

shyapa <commands>
